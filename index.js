//Import thư viện expressjs tương đương import express from "express"; 
const express = require("express");

//Khởi tạo 1 app express 
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//khai báo để app đọc được body json
app.use(express.json());

//Callback function là 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

//Request params (áp dụng cho GET, PUT, DELETE)
//Request params là những tham số xuất hiện trong url
//cần khai báo thêm dòng số 11
app.get("/request-params/:param1/:param2/param", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;
    res.json({
        param: {
            param1,
            param2
        }
    })
})

//Request Query (chỉ áp dụng cho phương thức GET)
//Khi lấy request query bắt buộc phải validate

app.get("/request-query", (req, res) => {
    let query = req.query;
    res.json({
        query
    })
})

//Request Body Json (chỉ áp dụng cho POST PUT)
//khi lấy request body bắt buộc phải validate
app.post("/request-body-json", (req, res) => {
    let body = req.body;
    res.json({
        body
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})